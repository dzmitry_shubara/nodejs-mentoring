import config from './config';
import { User, Product } from './models/index';
import { Dirwatcher, Importer } from './modules/index';

console.log(config.name);

const someUser = new User();
const someProduct = new Product();

const dirWatcher = new Dirwatcher();
const importer = new Importer();

const directoryPath = './data';
const delay = 3000;

try {
  dirWatcher.on('changed', (filePath) => {
    importer.import(filePath)
      .then((data) => {
        console.log(data);
    });
  });

  dirWatcher.on('changed', (filePath) => {
    console.log(importer.importSync(filePath));
  });

  dirWatcher.watch(directoryPath, delay);
}
catch (error) {
  throw new Error('reading path failing');
}
