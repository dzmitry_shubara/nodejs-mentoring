import fs from 'fs';
import EventEmitter from 'events';
import { promisify } from 'util';

const readDirAsync = promisify(fs.readdir);
const statFs = promisify(fs.stat);

export default class Dirwatcher extends EventEmitter {
  constructor() {
    super();
    this.filesInDir = new Map();
  }
  watch(path, delay) {
    if (!path) {
      console.log('The path is not specified!');
    }
    statFs(path).then((stats) => {
      setInterval(() => {
        readDirAsync(path).then((files, stats) => {
          files.forEach(name => {
            const filePath = `${path}/${name}`;
              statFs(filePath)
              .then((stats) => {
                this.checkAddFile(filePath, stats);
                this.checkDelFile(path, files);
                this.checkModFile(filePath, stats);
              });
          });
        });
      })
    }, delay);
  }

  checkModFile(filePath, stats){
    if (this.filesInDir.has(filePath) && this.filesInDir.get(filePath) !== stats.size) {
      this.filesInDir.set(filePath, stats.size);
      this.emit('changed', filePath);
      console.log(`file ${filePath} changed`);
    }
  }

  checkAddFile(filePath, stats) {
    if (!this.filesInDir.has(filePath)) {
      this.filesInDir.set(filePath, stats.size);
      this.emit('changed', filePath);
      console.log(`file ${filePath} added`);
    }
  }

  checkDelFile(path, files) {
    if (this.filesInDir.size !== files.length) {
      for(const filePath of this.filesInDir.keys()) {
        const fileName = filePath.substring(path.length + 1);
        if (!~files.indexOf(fileName)) {
          this.filesInDir.delete(filePath);
          console.log(`file ${filePath} deleted`);
        }
      }
    }
  }
}
