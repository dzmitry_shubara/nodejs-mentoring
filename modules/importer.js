import fs from 'fs';
import csv from 'csvtojson';

const Converter = csv.Converter;

export default class Importer {
  import(path){
    return new Promise((res, rej) => {
      const converter = new Converter({});
      converter.on('end_parsed', (jsonData) => {
        if(!jsonData){
          rej(new Error('CSV to JSON conversion failed!'));
        }
        res(jsonData);
      });
      fs.createReadStream(path).pipe(converter);
      })
  }

  importSync(path){
    const csvString = fs.readFileSync(path).toString();
    return Importer.convertToJSON(csvString);
  }

  static convertToJSON(csv) {
    const rows = csv.split('\n');
    const results = [];
    const headers = rows[0].split(',');
    rows.map((row, indexRow) => {
      if (indexRow < 1) return;
      const obj = {};
      const currentRow = row.split(',');
      headers.map((header, indexHeader) => {
        obj[header] = currentRow[indexHeader];
      });
      results.push(obj);
    });
    results.pop();
    return results;
  }
}
